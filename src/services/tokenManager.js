import { setAuthority } from '../utils/authority';
import jwtDecode from 'jwt-decode';
// import { reloadAuthorized } from '@/utils/Authorized';

const tokenManager = {
  set: key => {
    localStorage.setItem('token', key);
    setAuthority(jwtDecode(key).roles);
    // reloadAuthorized();
  },
  remove: () => {
    localStorage.removeItem('token');
    setAuthority([]);
    // reloadAuthorized();
  }
};

export default tokenManager;
