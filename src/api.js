import axios from 'axios'

// import {Profissional} from './models/profissional'
// import {CategoriaServico} from './models/servico'

// import {CategoriaServico, Servico} from '../models/servico'

// const BASE_URL = 'https://probook-web.herokuapp.com/api/'
// const BASE_URL = 'http://localhost:4000'
const BASE_URL = '/api'
const GET_TIMEOUT = 5000

const instance = axios.create(
  {
    baseURL: BASE_URL,
    timeout: GET_TIMEOUT
  })

export const jwt = {
  store: token => {
    localStorage.setItem('token', token)
  },
  retrive: () => {
    return localStorage.getItem('token')
  },
  axios: () => {
    const token = jwt.retrive()
    instance.interceptors.request.use(
      cfg => {
        cfg.headers.common['Authorization'] = `Bearer ${token}`
        return cfg
      }
    )

    instance.interceptors.response.use(res => res, error => {
      const { response } = error;
      switch (response.status) {
        case 401:
          /* eslint-disable no-underscore-dangle */
          window.g_app._store.dispatch({ type: 'auth/logout' })
            .then(() => router.push('/auth/login'));
          break;
        default:
          message.error(response.status);
          break;
      }
      return Promise.reject(error);
    });
  },
  setup: () => {
    const token = jwt.retrive()
    if (token) {
      jwt.axios()
    }
  }
}

jwt.setup()

const api = {
  // perfil () {
  login () {
    console.log('api - login')
    return instance
      .post('/login')
      .then(res => {
        return res.data
      })
    // return instance({
    //   method: 'post',
    //   url: '/login',
    //   data: formData,
    //   config: {
    //     headers: { 'Content-Type': 'multipart/form-data' }
    //   }
    // }).then(res => {
    //   const { authorization } = res.headers;
    //   const jwtString         = authorization.replace(/^Bearer /,"");
    //
    //   tokenManager.set(jwtString);
    // })
  },

  cli: {
    estabelecimentos: {
      profissionais: {
      }
    }
  }
}

export default api
